<div class="tour-holiday">
    <div class="container ">
        <div class="row ">
            <div class="col-md-5 ">
                <div class="info-holiday ">
                    <h5 class="sub-title ">
                        Trải nghiệm sắc xuân thế giới
                    </h5>
                    <h2 class="title ">
                        LỄ HỘI HOA 5 CHÂU
                    </h2>
                    <div class="text ">Thưởng ngoạn hoa anh đào trên đảo Jeju và thủ đô Seoul nơi xứ sở Kim Chi - Hàn Quốc. Khám phá thủ đô Seoul sầm uất, thưởng thức show diễn nghệ thuật Painters Hero vẽ tranh độc đáo trên nền nhạc sôi động kết hợp các điệu nhảy
                        vui nhộn
                    </div>
                    <a href="/blogs/all " class="btn-maincolor ">Xem thêm</a>
                </div>
            </div>
            <div class="col-md-7 img-holiday">
                <a href=" "><img src="https://bizweb.dktcdn.net/100/299/077/themes/642224/assets/sec_video_tour_embed_img.jpg?1607937814596 " alt=" "></a>
            </div>
        </div>
    </div>
</div>