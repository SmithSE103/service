       
       
       
 <footer>
    <div class="siter-footer ">
        <div class="container ">
            <div class="footer-inner padding-top-25 padding-bottom-10 ">
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-md-4 ">
                        <div class="footer-widget footer-contact-box ">
                            <h3>Liên Hệ</h3>
                            <div class="footer-widget-content ">
                                <div class="icon ">
                                    <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/fot_hotline.svg?1607937814596 " alt="Liên hệ ">
                                </div>
                                <div class="info ">
                                    <p class="questions ">Hỗ trợ trực tuyến 24/7!</p>
                                    <p class="phone ">
                                        Hotline:

                                        <a href="tel:0982362509 ">0982 362 509</a>

                                    </p>
                                    <p class="address ">

                                        175 Lý Thường Kiệt, Phường 6, Quận Tân Bình, TP Hồ Chí Minh

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-2 ">
                        <div class="footer-widget ">
                            <h3 class="toggle-service"> Dịch Vụ</h3>
                            <ul class="list-menu service-footer">
                                <li><a href=" ">Trang chủ</a></li>
                                <li><a href=" ">Giới thiệu</a></li>
                                <li><a href=" ">Tour trong nước</a></li>
                                <li><a href=" ">Tour nước ngoài</a></li>
                                <li><a href=" ">Dịch vụ tour</a></li>
                                <li><a href=" ">Cẩm nang du lịch</a></li>
                                <li><a href=" ">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                        <div class="footer-widget ">
                            <h3 class="toggle-support ">CHĂM SÓC KHÁCH HÀNG</h3>
                            <ul class="list-menu support-list">
                                <li><a href=" ">Trang chủ</a></li>
                                <li><a href=" ">Giới thiệu</a></li>
                                <li><a href=" ">Tour trong nước</a></li>
                                <li><a href=" ">Tour nước ngoài</a></li>
                                <li><a href=" ">Dịch vụ tour</a></li>
                                <li><a href=" ">Cẩm nang du lịch</a></li>
                                <li><a href=" ">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 ">
                        <div class="footer-widget ">
                            <h3 class="toggle-policy">CHÍNH SÁCH</h3>
                            <ul class="list-menu policy">
                                <li><a href=" ">Trang chủ</a></li>
                                <li><a href=" ">Giới thiệu</a></li>
                                <li><a href=" ">Tour trong nước</a></li>
                                <li><a href=" ">Tour nước ngoài</a></li>
                                <li><a href=" ">Dịch vụ tour</a></li>
                                <li><a href=" ">Cẩm nang du lịch</a></li>
                                <li><a href=" ">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="coppyright clearfix ">
        <div class="container">
            <div class="inner clearfix ">
                <div class="row">
                    <div class="col-md-12 text-center ">
                        <span>
                        © Bản quyền thuộc về 
                    <b>Minh Hieu</b>
                    <span class="s480-f">|</span> Cung cấp bởi
                        <a href="facebook.com/0609NMH" title="Sapo
                    " target="_blank" rel="nofollow ">HIEU</a>
                        </span>
                    </div>
                </div>

            </div>
            <div class="back-zalo">
                <a target="_blank" href="http://zalo.me/0982 362 509" title="Chat qua Zalo">
                    <span class="ti-zalo"></span>
                </a>
            </div>
            <div class="back-hotline">
                <button type="button" data-toggle="modal" data-target="#hotlineModal"><i class="fas fa-phone"></i></button>
            </div>
            <div class="back-to-top">
                <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-arrow-circle-up"></i></button>
            </div>
        </div>
    </div>
</footer>