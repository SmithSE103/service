@extends('admin.layouts.app')
@section('title', 'Danh sach san pham')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-success" title="add news"><a href="{{URL::to('/admin/product/create')}}"><i class="fas fa-plus"></i> Add Products</a></button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">ModelMachine</th>
            <th scope="col">Action</th>

        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
{{--        @if() @endif--}}

        @forelse($products as $pr)
            <tr>
                <th class="stt" scope="row">{{ $stt }}</th>
                <td><a>{{$pr->name }}</a></td>
                <td>{{ $pr->model_machines->name }}</td>
                <td>
                    <button class="btn btn-primary"><a href="{{route('product.edit',['id'=>$pr->id])}}"><i class="fas fa-pencil-alt " style="color: white"></i></a></button>
                    <button class="btn btn-danger"><a href="{{ route('product.destroy', ['id' => $pr->id]) }}"><i class="fas fa-trash-alt" style="color: white"></i></a></button>
                </td>

            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>

@stop
@section('script')
    <script src=""></script>
    <script>
        sdfhjashfa
    </script>
    @endsection
@section('style')
    @endsection

