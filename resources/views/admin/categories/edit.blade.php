@extends('admin.layouts.app')
@section('title', 'Edit the loai')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-warning" title="add news"><a href="{{URL::to('/admin/category')}}"><i class="fas fa-arrow-alt-circle-left"></i> Back</a></button>
    </div>
    <h1>{{ (session('message') ? session('message') : " ") }}</h1>
    <div class="error">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <form method="post" action="{{ route('category.update', ['id' => $category->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1 ">Name</label>
            <input type="text" value="{{$category->name}}" name="name" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER NAME">
        </div>
        <button type="submit" class="btn btn-primary margin-left-3px">Update</button>
    </form>

@stop

