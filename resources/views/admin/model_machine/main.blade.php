@extends('admin.layouts.app')
@section('title', 'Danh sach the loai')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-success" title="add news"><a href="{{URL::to('/admin/model_machine/create')}}"><i class="fas fa-plus"></i> Add ModelMachine</a></button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Name</th>
            <th scope="col">Category</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
        @forelse($model_machines as $c)
            <tr>
                <th class="stt" scope="row">{{ $stt }}</th>
                <td><a >{{$c->name }}</a></td>
                <td>{{$c->categories->name}}</td>
                <td>
                    <button class="btn btn-primary"><a href="{{route('model_machine.edit',['id'=>$c->id])}}"><i class="fas fa-pencil-alt " style="color: white"></i></a></button>
                    <button class="btn btn-danger"><a href="{{ route('model_machine.destroy', ['id' => $c->id]) }}"><i class="fas fa-trash-alt" style="color: white"></i></a></button>
                </td>

            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
    <style>

        td {
            width: calc((1130px)/4);
        }

    </style>

@stop

