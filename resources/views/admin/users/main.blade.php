@extends('admin.layouts.app')
@section('title', 'Danh sach san pham')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-success" title="add news"><a href="{{URL::to('/admin/users/create')}}"><i class="fas fa-plus"></i> Add Users</a></button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Permission</th>
            <th scope="col">Action</th>

        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
{{--        @if() @endif--}}

        @forelse($users as $u)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a>{{$u->name }}</a></td>
                <td><a>{{$u->email }}</a></td>
                <td><a><span class="label label-success">{{ implode(',', $u->roles()->get()->pluck('name')->toArray())}}</span></a></td>
{{--                    <td><button class="btn btn-danger">.</button></td>--}}

                <td>
{{--                    <button class="btn btn-warning"><a href="{{route('productbrand.edit',['id'=>$u->id])}}"><i class="fas fa-plus-circle " style="color: white"></i></a></button>--}}
                    <button class="btn btn-primary"><a href="{{route('users.edit',['id'=>$u->id])}}"><i class="fas fa-pencil-alt " style="color: white"></i></a></button>
                    <button class="btn btn-danger"><a href="{{ route('users.destroy', ['id' => $u->id]) }}"><i class="fas fa-trash-alt" style="color: white"></i></a></button>
                </td>

            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop
@section('script')
    <script src=""></script>
    <script>
        sdfhjashfa
    </script>
    @endsection
@section('style')
    @endsection

