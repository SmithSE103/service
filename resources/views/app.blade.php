<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/favicon.png?1607937814596" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('fontend/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" />
    <link rel="stylesheet" href="{{asset('fontend/css/reponsive.css')}}" >
    <link rel="stylesheet" href="{{asset('fontend/css/style.css')}} " />
   <link rel="stylesheet" href="{{asset('fontend/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('fontend/css/owl.theme.css')}}">

    <title>H NEWS</title>
</head>

<body>
    <div id="main">
      @include('layouts.header')
        <!--header-->
        @include('layouts.slider')
        <!--slider-->
        <hr>
        @include('layouts.service1')
        <hr>
        <!--service1-->
        @include('layouts.service2')
        <!--service2-->
        <hr>
        @include('layouts.service3')
        <!--service3-->
       @include('layouts.tour_holiday')
        <!--holiday-->
        @include('layouts.tour_location')
        <!--tour-holiday -->
      @include('layouts.blog')
        <!--blog-->
       @include('layouts.footer')
        <!--footer-->
    </div>
    <!--main-->
    <div class="backdrop__body-backdrop___1rvky active"></div>
    <ul class="the-article-tools">
        <li class="btnZalo zalo-share-button ">
            <a target="_blank " href="http://zalo.me/0965 198 897 " title="Chat qua Zalo">
                <span class="ti-zalo "></span>
            </a>
            <span class="label ">Chat qua Zalo</span>
        </li>
        <li class="btnFacebook ">
            <a target="_blank" href="https://www.messenger.com/0609nmh " title="Chat qua Messenger">
                <span class="ti-facebook "></span>
            </a>
            <span class="label">Chat qua Messenger</span>
        </li>
        <li class="btnphone ">
            <button type="button " data-toggle="modal" data-target="#hotlineModal">
                <span class="fas fa-phone"></span>
            </button>
            <span class="label ">Hotline đặt Tour</span>
        </li>
    </ul>
</body>
<script src="{{asset('fontend/js/script.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js " integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW " crossorigin="anonymous "></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js "></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js "></script>
<script src="{{asset('fontend/js/main.js')}}"></script>
<script src="{{asset('fontend/js/owl.carousel.min.js')}}"></script>



</html>