<?php

namespace Modules\News\Http\Controllers;
use App\Model\Category;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
//use App\Model\News;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\News\Entities\News;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $news = News::sortable()->paginate(5);
        return view('admin.new.main',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('admin.new.create')->with('warning',"Don't Open this link");
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
//    public function validateRequest(){
//        $ValidateDate = request()->validate([
//            'name' => 'required|min:3',
//            'image' => 'required|image',
//
//        ]);
//        if (request()->hasFile('image')) {
//            dd(request()->image);
//            request()->validate([
//                'image' => 'required|image',
//            ]);
//        }
//        return $ValidateDate;
//    }
    public function store(Request $request)
    {
//        $request->validate([
//            'image' => 'required',
//        ]);
//        if($request->hasFile('image'))
//        {
//            $image_name = $request->file('image')->getClientOriginalName();
//            $filename = pathinfo($image_name,PATHINFO_FILENAME);
//            $image_ext = $request->file('image')->getClientOriginalExtension();
//            $fileNameToStore = $filename.'-'.time().'.'.$image_ext;
//            $path = $request->file('image')->storeAs('public',$fileNameToStore);
//
//        }
//        else{
//            $fileNameToStore = 'noimage.jpg';
//        }
//        $news = new News();
//        $news->fill($request->all());
//
////        $data->image = $fileNameToStore;
////        dd($news);
//        $news->save();
//
//        $request->validate([
//            'name'=>'required',
//            'image' => 'required',
//            'title'=> 'requireds'
//        ]);
        $path = null;
        $data = $request->all();
        if ($request->file('image')) {
            $path = $request->file('image')->store('public/');
            $data['image'] = $path;
        }
        News::create($data);
//        dd($data);
//        $news->save($data);


        return  redirect('/admin/news')->with('info','You added new items, follow next step!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        News::find($id)->delete();

        return redirect()->back()->with('success',"Deleted successfully");
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        return view('admin.new.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'image' => 'nullable',
        ]);

        $news = News::find($id);

        $path = null;
        if ($request->file('image')) {
            $path = $request->file('image')->store('public');
            @unlink('storage/'. $news->image);
        }

        $data = $request->all();
        $data['image'] = $path;

//        dd($data);
        $news->update($data);

        return redirect('/admin/news');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {

        News::find($id)->delete();

        return redirect()->back();
    }
}
