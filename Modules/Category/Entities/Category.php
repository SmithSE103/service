<?php

namespace Modules\Category\Entities;
use Modules\ModelMachine\Entities\ModelMachine;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Category extends Model
{
    use Sortable;
    protected $table = 'categories';
    protected $fillable = [
        'id', 'name'
    ];
    public $sortable = ['name'];
    public function Model_machines()
    {
        return $this->hasMany('Modules\ModelMachine\Entities\ModelMachine', 'categories_id');
    }
//    public function getSearchResult():SearchResult{
//            $url = route('category.show',$this->id);
//            return new SearchResult(
//                $this,
//                $this->name,
//                $url
//            );
//    }
}
