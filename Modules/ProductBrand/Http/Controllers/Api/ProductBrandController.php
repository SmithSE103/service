<?php

namespace Modules\ProductBrand\Http\Controllers\Api;

use Modules\ProductBrand\Entities\ProductBrand;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\ProductBrand\Transformers\ProductBrandResource;

class ProductBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        header('Access-Control-Allow-Origin: *');
        $product_brands = ProductBrand::with('model_machines')->orderByDesc('id')->get();
        // $modelmachines = Modelmachine::with('product_brands')->orderByDesc('id')->get();
        // dd($modelmachines);
        (function ($product_brands) {
        $product_brands->image = str_replace('public', '', $product_brands->image);

        return $product_brands;
    });
        return ProductBrandResource::collection($product_brands);
    }

    public function findID($id)
    {
        header('Access-Control-Allow-Origin: *');
        $product_brands = ProductBrand::with('model_machines')->find($id);
        return new ProductBrandResource($product_brands);
    }
}
