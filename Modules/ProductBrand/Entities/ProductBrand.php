<?php

namespace Modules\ProductBrand\Entities;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ProductBrand extends Model
{
    use Sortable;
    protected $table = 'product_brands';
    protected $fillable = [
        'id', 'name', 'price','price_old', 'sale_off','status', 'desc','image', 'model_machines_id'
    ];
    public $sortable = ['id', 'name', 'price','price_old', 'sale_off','status', 'created_at', 'updated_at'];
    function Model_machines(){
        return $this->belongsTo('Modules\ModelMachine\Entities\ModelMachine', 'model_machines_id');
    }
}
