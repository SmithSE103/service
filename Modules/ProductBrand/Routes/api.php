<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/product_brand', function (Request $request) {
    return $request->user();
});
Route::prefix('product_brand')->group( function () {
    Route::get('/', 'Api\ProductBrandController@index');
    Route::get('/find/{id}', 'Api\ProductBrandController@findID');
});
