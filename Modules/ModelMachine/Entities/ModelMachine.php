<?php

namespace Modules\ModelMachine\Entities;

use Illuminate\Database\Eloquent\Model;

class ModelMachine extends Model
{
    protected $table = 'model_machines';
    protected $fillable = [
        'id','name','categories_id'
    ];
    public function Categories()
    {
        return $this->belongsTo('Modules\Category\Entities\Category', 'categories_id');

    }
    public function product_brands()
    {
        return $this->hasMany('Modules\ProductBrand\Entities\ProductBrand', 'model_machines_id');
    }
    public function products()
    {
        return $this->hasMany('Modules\Product\Entities\Product', 'model_machines_id');
    }
}
