<?php

namespace Modules\ModelMachine\Http\Controllers;

//use App\Model\Category;
use Modules\Category\Entities\Category;

//use App\Model\ModelMachine;
use Modules\ModelMachine\Entities\ModelMachine;
use Illuminate\Http\Response;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class  ModelMachineController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
//    public function search($categories_id,$model_machine_name)
//    {
//        header('Access-Control-Allow-Origin: *');
////        header('Access-Control-Allow-Origin: true');
////        $categories = Category::find($categories_id);
//        $model_machine_name = ModelMachine::where('categories_id',$categories_id)->where('name','like','%'.$model_machine_name.'%')->get();
////        dd($categories);
////        return response()->json($product);
//
//        if(count($model_machine_name) > 1){
//            $response = [
//                'data'=> $model_machine_name,
//                'status' => Response::HTTP_OK,
//            ];
//            return response()->json($response);
//        } else{
//            $response = [
//                'data'=> $model_machine_name,
//                'status' => Response::HTTP_NOT_FOUND,
//            ];
//            return response()->json($response, Response::HTTP_NOT_FOUND);
//        }
//    }
    public function index(Request $request)
    {
//            $param_search = $request->get('q');
//
//            if (isset($param_search) && $request->has('q')) {
//                $model_machines = ModelMachine::with('categories')->where('name', 'like', '%'. $param_search . '%')->get();
//            }else {
//                $model_machines = ModelMachine::with('categories')->orderByDesc('id')->get();
//            }
        $model_machines = ModelMachine::with('categories')->orderByDesc('id')->get();
        return view('admin.model_machine.main',compact('model_machines'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = Category::all();
//
        return view('admin.model_machine.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $model_machines = new ModelMachine($request->all());
        $model_machines->save();
        return redirect('/admin/model_machine');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        ModelMachine::find($id)->delete();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $model_machines = ModelMachine::findOrFail($id);
        $categories = Category::all();

        return view('admin.model_machine.edit', compact('categories','model_machines'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $model_machines = ModelMachine::find($id);
        $model_machines-> update($request->all());
        return redirect('/admin/model_machine');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        ModelMachine::find($id)->delete();

        return redirect()->back();

    }
}
