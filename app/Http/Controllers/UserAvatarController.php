<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class UserAvatarController extends Controller
{
    public function update(Request $request)
    {
        $path = $request->file('avatar')->store('avatars');
        $disk = Storage::disk('gcs');
        $url = $disk->url('folder/my_file.txt');
        return $path;
    }
}
