<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
//use Google_Client;
//use Illuminate\Support\Facades\Storage;
//use League\Flysystem\Adapter\AbstractAdapter;
////use Illuminate\Support\Facades\Storage;
use Storage;
class StorageController extends Controller
{

    public function uploadimage(){
//        $filename = ' drive_image';
//        $filePath = public_path('fontend/img/slider_1.jpg');
//        $fileData = File::get($filePath);
        $googleDriveStorage = Storage::disk('google_drive');
        $googleDriveStorage->put('slider_1.jpg', 'Hello world');
//        dd($googleDriveStorage);
//        Storage::cloud()->put($fileData,$filename);
        return 'Image Uploaded';
    }
    public function list_document(){
//        $googleDriveStorage = Storage::disk('google_drive');
//        $fileinfo = collect($googleDriveStorage->listContents('/', false))
//            ->where('type', 'file')
//            ->where('name', 'test.txt')
//            ->first();
//
//// Đọc nội dung file 'test.txt' mà mình đã tạo ở trên
//        $contents = $googleDriveStorage->get($fileinfo['path']);
//
//        dd($contents); // Hello world
        $dir = '/';
        $recursive = false;
        $contents = json_decode(collect(Storage::disk('google_drive')->listContents($dir,$recursive)));
        dd($contents);

    }
    public function createfolder(){
        $googleDriveStorage = Storage::disk('google_drive');

        $googleDriveStorage->makeDirectory('news13');
        return 'Create Folder Success ';
    }
    public function deletefolder(){
        $googleDriveStorage = Storage::disk('google_drive');

            // Đầu tiên cần lấy ra thông tin của folder 'avatars'
            // trên google drive trước đã
        $folderinfo = collect($googleDriveStorage->listContents('/', false))
            ->where('type', 'dir')
            ->where('name', 'news1')
            ->first();

        $googleDriveStorage->deleteDirectory($folderinfo['path']);
        return 'Delete Folder Success';
    }
    public function renamefolder(){
        $googleDriveStorage = Storage::disk('google_drive');

            // Đầu tiên cần lấy ra thông tin của folder 'avatars'
            // trên google drive trước đã
        $folderinfo = collect($googleDriveStorage->listContents('/', false))
            ->where('type', 'dir')
            ->where('name', 'news13')
            ->first();

        // Đổi tên từ 'avatars' thành 'images'
        $googleDriveStorage->move($folderinfo['path'], 'news12');
    }
}
