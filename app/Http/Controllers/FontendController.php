<?php

namespace App\Http\Controllers;

use Modules\Category\Entities\Category;
use Modules\ModelMachine\Entities\ModelMachine;
use App\Model\Product;
use Illuminate\Http\Request;

class FontendController extends Controller
{
        public function index(){
            $categories = Category::with('model_machines')->orderByDesc('id')->get();

            return view('app',compact('categories'));
        }
}
