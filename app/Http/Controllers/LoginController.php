<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    // hanh dong ..
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

//        dd($credentials);

        if (Auth::attempt($credentials)) {

            return redirect()->intended('admin')->with('success','Login successfully!');
        }else {
//            dd("sai");
            return redirect()->back()->with('warning',"Don't Open this link");;
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login')->with('success','Logout successfully!');
    }
}
